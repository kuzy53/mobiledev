package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class Main : AppCompatActivity() {

    // Этот метод вызывается при создании активности
    override fun onCreate(savedInstanceState: Bundle?) {
        // Вызов метода onCreate() у родительского класса
        super.onCreate(savedInstanceState)
        // Устанавливаем макет, описывающий внешний вид активности
        setContentView(R.layout.activity_main)
    }
}