package com.example.myapplication

import RectAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.google.android.material.floatingactionbutton.FloatingActionButton

// создаем класс RectFragment, который является фрагментом
class RectFragment : Fragment() {

    // объявляем переменные, которые будут использоваться в классе
    private var recycler: RecyclerView? = null
    private var adapter: RecyclerView.Adapter<*>? = null
    private var manager: RecyclerView.LayoutManager? = null

    // метод onCreateView вызывается при создании и настройке интерфейса фрагмента
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // создаем и возвращаем представление фрагмента, заданное в R.layout.fragment_rectangles
        val rootView = inflater.inflate(R.layout.fragment_rectangles, container, false)
        return rootView
    }

    // метод onViewCreated вызывается после того, как представление фрагмента создано и представлено на экране
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // создаем экземпляр GridLayoutManager с заданным количеством столбцов
        manager = GridLayoutManager(requireContext(), calculateColumns())

        // создаем экземпляр адаптера RectAdapter и присваиваем его переменной adapter
        adapter = RectAdapter()

        // находим RecyclerView по id и присваиваем его переменной recycler
        recycler = view.findViewById<RecyclerView>(R.id.rectangles_recycler_view).also {
            // задаем фиксированный размер RecyclerView для повышения производительности
            it.setHasFixedSize(true)
            // устанавливаем LayoutManager для RecyclerView
            it.layoutManager = manager
            // устанавливаем адаптер для RecyclerView
            it.adapter = adapter
        }

        // находим FloatingActionButton по id и устанавливаем на него обработчик нажатия
        view.findViewById<FloatingActionButton>(R.id.add_rectangle_button).setOnClickListener {
            // вызываем метод addRectangle() адаптера RectAdapter, чтобы добавить новый элемент в RecyclerView
            (adapter as RectAdapter).addRectangle()
        }
    }

    // метод calculateColumns() возвращает количество столбцов в RecyclerView в зависимости от ориентации экрана
    private fun calculateColumns(): Int {
        val orientation = resources.configuration.orientation
        return when (orientation) {
            android.content.res.Configuration.ORIENTATION_PORTRAIT -> 3
            else -> 4
        }
    }
}
