import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.example.myapplication.R

// создаем класс адаптера, который расширяет RecyclerView.Adapter
class RectAdapter : RecyclerView.Adapter<RectAdapter.ViewHolder>() {

    // определяем список прямоугольников
    private val rectangles = mutableListOf<Int>().apply {
        // заполняем его числами от 0 до 19
        for (i in 0 until 20) {
            add(i)
        }
    }

    // создаем класс ViewHolder
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // инициализируем TextView, который отображает номер прямоугольника
        val number: TextView = view.findViewById(R.id.rectangle_number)
    }

    // создаем новый ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // используем LayoutInflater, чтобы создать новый View из layout-файла item_rectangle
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_rectangle, parent, false)
        // создаем и возвращаем новый ViewHolder на основе этого View
        return ViewHolder(view)
    }

    // связываем ViewHolder с данными
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // устанавливаем текст TextView из списка прямоугольников по данной позиции
        holder.number.text = rectangles[position].toString()

        // выбираем цвет фона TextView на основе позиции в списке
        val color = when (position % 2) {
            0 -> android.R.color.holo_red_light
            else -> android.R.color.holo_blue_light
        }
        // устанавливаем фон TextView на основе выбранного цвета
        holder.number.setBackgroundResource(color)
    }

    // возвращает количество элементов в списке прямоугольников
    override fun getItemCount() = rectangles.size

    // добавляет новый прямоугольник в список
    fun addRectangle() {
        // увеличиваем список на 1 элемент и добавляем туда новый номер прямоугольника
        rectangles += rectangles.size
        // уведомляем адаптер, что был добавлен новый элемент
        notifyItemInserted(rectangles.lastIndex)
    }
}
